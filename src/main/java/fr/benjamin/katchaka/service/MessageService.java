package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Message;

public interface MessageService {

	Message creerMessage(Message message);
	
	Optional<Message> recupererMessage(Long idMessage);
	
	List<Message> recupererMessages();
	
	Message majMessage(Message message);
	
	boolean supprimerMessage(Long idMessage);
}
