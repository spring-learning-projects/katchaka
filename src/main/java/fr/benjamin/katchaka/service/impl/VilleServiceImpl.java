package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Ville;
import fr.benjamin.katchaka.dao.VilleDao;
import fr.benjamin.katchaka.service.VilleService;

@Service
public class VilleServiceImpl implements VilleService {

	@Autowired
	private VilleDao villeDao;
	
	@Override
	public Ville creerVille(Ville ville) {
		return villeDao.save(ville);
	}

	@Override
	public Optional<Ville> recupererVille(Long idVille) {
		return villeDao.findById(idVille);
	}

	@Override
	public List<Ville> recupererVilles() {
		return villeDao.findAll();
	}

	@Override
	public Ville majVille(Ville ville) {
		return villeDao.save(ville);
	}

	@Override
	public boolean supprimerVille(Long idVille) {
		Ville ville = recupererVille(idVille).get();
		if (ville == null) {
			return false;
		} else {
			villeDao.deleteById(idVille);
			return true;
		}
	}

}
