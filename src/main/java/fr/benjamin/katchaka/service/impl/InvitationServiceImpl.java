package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Invitation;
import fr.benjamin.katchaka.business.Personne;
import fr.benjamin.katchaka.dao.InvitationDao;
import fr.benjamin.katchaka.service.InvitationService;

@Service
public class InvitationServiceImpl implements InvitationService {

	@Autowired
	private InvitationDao invitationDao;
	
	@Override
	public Invitation creerInvitation(Invitation invitation) {
		return invitationDao.save(invitation);
	}

	@Override
	public Optional<Invitation> recupererInvitation(Long idInvitation) {
		return invitationDao.findById(idInvitation);
	}

	@Override
	public List<Invitation> recupererInvitations() {
		return invitationDao.findAll();
	}
	
	@Override
	public List<Invitation> recupererInvitationsByDestinataire(Personne destinataire) {
		return invitationDao.findAllByDestinataire(destinataire);
	}
	
	@Override
	public List<Invitation> recupererInvitationsByExpediteur(Personne expediteur) {
		return invitationDao.findAllByExpediteur(expediteur);
	}

	@Override
	public Invitation majInvitation(Invitation invitation) {
		return invitationDao.save(invitation);
	}

	@Override
	public boolean supprimerInvitation(Long idInvitation) {
		Invitation invitation = recupererInvitation(idInvitation).get();
		if (invitation == null) {
			return false;
		} else {
			invitationDao.deleteById(idInvitation);
			return true;
		}
	}

}
