package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.VieCommune;
import fr.benjamin.katchaka.dao.VieCommuneDao;
import fr.benjamin.katchaka.service.VieCommuneService;

@Service
public class VieCommuneServiceImpl implements VieCommuneService {

	@Autowired
	private VieCommuneDao vieCommuneDao;
	
	@Override
	public VieCommune creerVieCommune(VieCommune vieCommune) {
		return vieCommuneDao.save(vieCommune);
	}

	@Override
	public Optional<VieCommune> recupererVieCommune(Long idVieCommune) {
		return vieCommuneDao.findById(idVieCommune);
	}

	@Override
	public List<VieCommune> recupererVieCommunes() {
		return vieCommuneDao.findAll();
	}

	@Override
	public VieCommune majVieCommune(VieCommune vieCommune) {
		return vieCommuneDao.save(vieCommune);
	}

	@Override
	public boolean supprimerVieCommune(Long idVieCommune) {
		VieCommune vieCommune = recupererVieCommune(idVieCommune).get();
		if (vieCommune == null) {
			return false;
		} else {
			vieCommuneDao.deleteById(idVieCommune);
			return true;
		}
	}

}
