package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Interet;

public interface InteretService {

	Interet creerInteret(Interet interet);
	
	Optional<Interet> recupererInteret(Long idInteret);
	
	List<Interet> recupererInterets();
	
	Interet majInteret(Interet genre);
	
	boolean supprimerInteret(Long idInteret);
}
