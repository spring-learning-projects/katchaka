package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.VieCommune;

public interface VieCommuneService {

	VieCommune creerVieCommune(VieCommune vieCommune);
	
	Optional<VieCommune> recupererVieCommune(Long idVieCommune);
	
	List<VieCommune> recupererVieCommunes();
	
	VieCommune majVieCommune(VieCommune vieCommune);
	
	boolean supprimerVieCommune(Long idVieCommune);
}
