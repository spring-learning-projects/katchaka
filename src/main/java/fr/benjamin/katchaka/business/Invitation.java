package fr.benjamin.katchaka.business;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Invitation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String contenu;
	
	private Date dateEnvoi;
	
	private Date dateLecture;
	
	private Boolean estAcceptee;
	
	@ManyToOne
	private Personne expediteur;
	
	@ManyToOne
	private Personne destinataire;
	
	@OneToOne
	private VieCommune vieCommune;

	public Invitation() {
	}
	
	public Invitation(String contenu, Date dateLecture, Boolean estAcceptee, Personne expediteur, Personne destinataire) {
		super();
		this.contenu = contenu;
		this.dateEnvoi = new Date();
		this.dateLecture = dateLecture;
		this.estAcceptee = estAcceptee;
		this.expediteur = expediteur;
		this.destinataire = destinataire;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Date getDateLecture() {
		return dateLecture;
	}

	public void setDateLecture(Date dateLecture) {
		this.dateLecture = dateLecture;
	}

	public Boolean getEstAcceptee() {
		return estAcceptee;
	}

	public void setEstAcceptee(Boolean estAcceptee) {
		this.estAcceptee = estAcceptee;
	}

	public Personne getExpediteur() {
		return expediteur;
	}

	public void setExpediteur(Personne expediteur) {
		this.expediteur = expediteur;
	}

	public Personne getDestinataire() {
		return destinataire;
	}

	public void setDestinataire(Personne destinataire) {
		this.destinataire = destinataire;
	}

	public VieCommune getVieCommune() {
		return vieCommune;
	}

	public void setVieCommune(VieCommune vieCommune) {
		this.vieCommune = vieCommune;
	}

	@Override
	public String toString() {
		return "Invitation [id=" + id + ", contenu=" + contenu + ", dateEnvoi=" + dateEnvoi
				+ ", dateLecture=" + dateLecture + ", estAcceptee=" + estAcceptee + ", personne=" + expediteur
				+ ", destinataire=" + destinataire + ", vieCommune=" + vieCommune + "]";
	}
	
	
}
