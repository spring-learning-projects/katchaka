package fr.benjamin.katchaka.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import fr.benjamin.katchaka.business.Genre;
import fr.benjamin.katchaka.business.Interet;
import fr.benjamin.katchaka.business.Invitation;
import fr.benjamin.katchaka.business.Personne;
import fr.benjamin.katchaka.business.Statut;
import fr.benjamin.katchaka.business.Ville;
import fr.benjamin.katchaka.service.GenreService;
import fr.benjamin.katchaka.service.InteretService;
import fr.benjamin.katchaka.service.InvitationService;
import fr.benjamin.katchaka.service.PersonneService;
import fr.benjamin.katchaka.service.StatutService;
import fr.benjamin.katchaka.service.VilleService;


@Controller
@RequestMapping("/")
public class mainController {

	private static final int NB_PROFILS_PAR_PAGE = 5;
	
	private PersonneService personneService;
	private VilleService villeService;
	private GenreService genreService;
	private StatutService statutService;
	private InteretService interetService;
	private InvitationService invitationService;
	private HttpSession httpSession;
	
	private static Calendar calendar = Calendar.getInstance();

	public mainController(PersonneService personneService, VilleService villeService, GenreService genreService,
			StatutService statutService, InteretService interetService, HttpSession httpSession, InvitationService invitationService) {
		super();
		this.personneService = personneService;
		this.villeService = villeService;
		this.genreService = genreService;
		this.statutService = statutService;
		this.interetService = interetService;
		this.httpSession = httpSession;
		this.invitationService = invitationService;
	}
	
	//===============================================================================
	//=================================== Index =====================================
	@RequestMapping(value = {"/", "/index"})
	public ModelAndView connexion() {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("connexion");
			return mav;
		}
	}
	
	//===============================================================================
	//================================== Connexion ==================================
	@PostMapping("/connexion")
	public ModelAndView connexionPost(@RequestParam("IDENTIFIANT") String identifiant, @RequestParam("MOT_DE_PASSE") String motDePasse) {
		Personne personne = personneService.recupererPersonneByEmailAndMotDePasse(identifiant, motDePasse);
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			httpSession.setAttribute("personne", personne);
			return mav;
		} else {
			ModelAndView mav = connexion();
			mav.addObject("notification", "Le mot de passe et/ou l'identifiant est incorrect.");
			return mav;
		}
	}
	
	//===============================================================================
	//=================================== Accueil ===================================
	@RequestMapping("/accueil")
	public ModelAndView accueilGet(@PageableDefault(size = NB_PROFILS_PAR_PAGE) Pageable pageable, @RequestParam Map<String, Object> map) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("accueil");

			Genre genreRecherche = personne.getGenreRecherche();
	        Ville villeRecherchee = null;
	        Interet interetRecherche = null;
	        
	        if (map.get("ID_VILLE")!=null && !map.get("ID_VILLE").equals("")) {
	            villeRecherchee = villeService.recupererVille(Long.parseLong(map.get("ID_VILLE").toString())).get();
	            mav.addObject("villeRecherchee", villeRecherchee);
	        }

	        if (map.get("ID_INTERET")!=null && !map.get("ID_INTERET").equals("")) {
	            interetRecherche = interetService.recupererInteret(Long.parseLong(map.get("ID_INTERET").toString())).get();
	            mav.addObject("interetRecherche", interetRecherche);
	        }
	        
	        Sort.Order order = null;
	        Sort sort = pageable.getSort();
	        if (sort != null) {
	             if (sort.iterator().hasNext()) {
	                 order = sort.iterator().next();
	             }
	        }
	        
	        if (order != null) {
	             mav.addObject("sort", order.getProperty() + "," + order.getDirection().toString());
	        }
	        
	        mav.addObject("villes", villeService.recupererVilles());
			mav.addObject("interets", interetService.recupererInterets());
	        mav.addObject("personnesParPages", personneService.recupererPersonnes(pageable, villeRecherchee, interetRecherche, genreRecherche));
	        return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}
	}
	
	//===============================================================================
	//================================= Inscription =================================
	@GetMapping("/inscription")
	public ModelAndView inscriptionGet() {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("inscription");
			mav.addObject("personne", new Personne());
			mav.addObject("villes", villeService.recupererVilles());
			mav.addObject("genres", genreService.recupererGenres());
			mav.addObject("statuts", statutService.recupererStatuts());
			mav.addObject("interets", interetService.recupererInterets());
			return mav;
		}	
	}
	
	@PostMapping("/inscription")
	public ModelAndView inscriptionPost(@Valid @ModelAttribute Personne personne, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = inscriptionGet();
			System.out.println(personne);
			mav.addObject("personne", personne);
			return mav;
		}
		else  {
			System.out.println(personne);
			personneService.creerPersonne(personne);
			httpSession.setAttribute("personne", personne);
			ModelAndView mav = new ModelAndView("redirect:accueil");
			return mav;
		}
	}
	
	//===============================================================================
	//=============================== liste invitation ==============================
	@GetMapping("/liste-invitation")
	public ModelAndView listeInvitation() {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("listeInvitation");
			mav.addObject("listeInvitationRecues", invitationService.recupererInvitationsByDestinataire(personne));
			mav.addObject("listeInvitationEnvoyees", invitationService.recupererInvitationsByExpediteur(personne));
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	@GetMapping("/accepterInvitation")
	public ModelAndView accepterInvitation(@RequestParam("ID") Long idInvitation) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:liste-invitation");
			Invitation invitation = invitationService.recupererInvitation(idInvitation).get();
			invitation.setDateLecture(new Date());
			invitation.setEstAcceptee(true);
			invitationService.majInvitation(invitation);
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	@GetMapping("/declinerInvitation")
	public ModelAndView declinerInvitation(@RequestParam("ID") Long idInvitation) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:liste-invitation");
			Invitation invitation = invitationService.recupererInvitation(idInvitation).get();
			invitation.setDateLecture(new Date());
			invitation.setEstAcceptee(false);
			invitationService.majInvitation(invitation);
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	//===============================================================================
	//============================= Voir / Inviter profil ===========================
	@GetMapping("/profil")
	public ModelAndView profilGet(@RequestParam("ID") Long idPersonne) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("profil");
			mav.addObject("profil", personneService.recupererPersonne(idPersonne).get());
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	@GetMapping("/inviter")
	public ModelAndView inviterGet(@RequestParam("ID") Long idPersonne) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("inviter");
			mav.addObject("profil", personneService.recupererPersonne(idPersonne).get());
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	@PostMapping("/inviter")
	public ModelAndView inviterPost(@RequestParam("MESSAGE") String message, @RequestParam("destinataire") Long idDestinataire) {
		Personne personne = (Personne) httpSession.getAttribute("personne");
		if (personne != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			Personne destinataire = personneService.recupererPersonne(idDestinataire).get();
			Invitation invitation = new Invitation(message, null, null, personne, destinataire);
			invitationService.creerInvitation(invitation);
			mav.addObject("invitation", "L'invitation a bien été envoyé.");
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:");
			return mav;
		}	
	}
	
	//===============================================================================
	//================================= deconnexion =================================
	@GetMapping("/deconnexion")
	public ModelAndView deconnexion() {
		httpSession.invalidate();
		ModelAndView mav = new ModelAndView("redirect:");
		return mav;
	}
	
	//===============================================================================
	//============================== init / initBinder ==============================
	@Scheduled(cron="* * * * * *")
    private void ajouterPersonnesAleatoire() {
        System.out.println("ajoutPersonne");
    }
	
	@PostConstruct
	private void init() {
		
		villeService.creerVille(new Ville("Lyon"));
		villeService.creerVille(new Ville("Clermont-Ferrand"));
		villeService.creerVille(new Ville("Grenoble"));
		villeService.creerVille(new Ville("Paris"));
		
		genreService.creerGenre(new Genre("Homme"));
		genreService.creerGenre(new Genre("Femme"));
		
		statutService.creerStatut(new Statut("Celibataire"));
		statutService.creerStatut(new Statut("Separé(e)"));
		statutService.creerStatut(new Statut("Divorcé(e)"));
		statutService.creerStatut(new Statut("Veuf/veuve"));
		
		interetService.creerInteret(new Interet("Voyage"));
		interetService.creerInteret(new Interet("Restaurant"));
		interetService.creerInteret(new Interet("Sorties entre amis"));
		
		List<Interet> interets = interetService.recupererInterets();
		
		calendar.set(1995, 11, 15);
		personneService.creerPersonne(new Personne("benjamin@gmail.com", "benjamin", calendar.getTime(), "Benjamin", "C'est benjamin parsy", false, villeService.recupererVille(1l).get(), statutService.recupererStatut(1l).get(), genreService.recupererGenre(1l).get(), genreService.recupererGenre(2l).get(), interets));
		calendar.set(1997, 07, 15);
		personneService.creerPersonne(new Personne("jean@gmail.com", "jeanvaljean", calendar.getTime(), "Jean", "C'est jean Valjean le renegat", false, villeService.recupererVille(3l).get(), statutService.recupererStatut(2l).get(), genreService.recupererGenre(1l).get(), genreService.recupererGenre(1l).get(), interets));
		calendar.set(1996, 02, 18);
		personneService.creerPersonne(new Personne("fanny@gmail.com", "fanny", calendar.getTime(), "Fanny", "Je m'appelle fanny boyer", false, villeService.recupererVille(2l).get(), statutService.recupererStatut(1l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
		calendar.set(2000, 07, 12);
		personneService.creerPersonne(new Personne("lucas@gmail.com", "lucas", calendar.getTime(), "Lucas", "Je m'appelle lucas parsy", false, villeService.recupererVille(2l).get(), statutService.recupererStatut(1l).get(), genreService.recupererGenre(1l).get(), genreService.recupererGenre(2l).get(), interets));
		calendar.set(1996, 04, 23);
		personneService.creerPersonne(new Personne("manon@gmail.com", "manon", calendar.getTime(), "Manon", "Je m'appelle manon boyer", false, villeService.recupererVille(4l).get(), statutService.recupererStatut(2l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
		calendar.set(1997, 11, 28);
		personneService.creerPersonne(new Personne("laura@gmail.com", "laura", calendar.getTime(), "Laura", "Je m'appelle laura dupont", false, villeService.recupererVille(4l).get(), statutService.recupererStatut(2l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
		calendar.set(1992, 07, 07);
		personneService.creerPersonne(new Personne("virginie@gmail.com", "virginie", calendar.getTime(), "Virginie", "Je m'appelle virginie durez", false, villeService.recupererVille(4l).get(), statutService.recupererStatut(2l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
		
		for (int i = 1; i < 15; i++) {
			personneService.creerPersonne(new Personne("fanny"+i+"@gmail.com", "fanny"+i, calendar.getTime(), "Fanny", "Je m'appelle fanny" +i+ " boyer", false, villeService.recupererVille(2l).get(), statutService.recupererStatut(1l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
			personneService.creerPersonne(new Personne("manon"+i+"@gmail.com", "manon"+i, calendar.getTime(), "Manon", "Je m'appelle manon" +i+ " dupont", false, villeService.recupererVille(1l).get(), statutService.recupererStatut(2l).get(), genreService.recupererGenre(2l).get(), genreService.recupererGenre(1l).get(), interets));
		}
		
		invitationService.creerInvitation(new Invitation(null, null, null, personneService.recupererPersonne(3l).get(), personneService.recupererPersonne(1l).get()));
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(simpleDateFormat, true));
		binder.registerCustomEditor(List.class, "interets", new CustomCollectionEditor(List.class) {
			 @Override
			 public Object convertElement(Object objet) {
				 Long id = Long.parseLong((String) objet);
				 return interetService.recupererInteret(id).get();
			 }
		 });
	}
}
