package fr.benjamin.katchaka.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Genre;
import fr.benjamin.katchaka.business.Interet;
import fr.benjamin.katchaka.business.Personne;
import fr.benjamin.katchaka.business.Ville;

public interface PersonneDao extends JpaRepository<Personne, Long> {

	Personne findByEmailAndMotDePasse(String email, String motDePasse);
	
	Page<Personne> findAllByGenre(Pageable pageable, Genre genre);
	
	Page<Personne> findAllByVilleAndGenre(Pageable pageable, Ville villeRecherchee, Genre genreRecherche);

	Page<Personne> findAllByInteretsAndGenre(Pageable pageable, Interet interetRecherche, Genre genreRecherche);
	
	Page<Personne> findAllByVilleAndInteretsAndGenre(Pageable pageable, Ville ville, Interet interet, Genre genreRecherche);
}
