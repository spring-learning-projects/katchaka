package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Message;

public interface MessageDao extends JpaRepository<Message, Long> {

}
