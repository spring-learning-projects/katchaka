package fr.benjamin.katchaka.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Invitation;
import fr.benjamin.katchaka.business.Personne;

public interface InvitationDao extends JpaRepository<Invitation, Long> {

	List<Invitation> findAllByDestinataire(Personne destinataire);
	
	List<Invitation> findAllByExpediteur(Personne expediteur);
}
