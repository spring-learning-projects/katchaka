<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Inscription</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
		<main>
			<h1>Inscription</h1>
			
			<form:form modelAttribute="personne" action="inscription" method="post">
				
				<div class="form-group">
					<form:label path="pseudo">Pseudo :</form:label>
					<form:input class="form-control" path="pseudo"/>
					<form:errors path="pseudo" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="email">Email :</form:label>
					<form:input class="form-control" path="email"/>
					<form:errors path="email" cssClass="erreur" />
				</div>
			
				<div class="form-group">
					<form:label path="motDePasse">Mot de passe :</form:label>
					<form:password class="form-control" path="motDePasse"/>
					<form:errors path="motDePasse" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="ville.id">Ville : </form:label>
					<form:select class="form-control" path="ville.id">
						<form:option value="">Ville</form:option>
						<form:options items="${villes}" itemValue="id" itemLabel="nom" />
						<form:options/>
					</form:select>
					<form:errors path="ville.id" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="genre.id">Genre : </form:label>
					<form:select class="form-control" path="genre.id">
						<form:option value="">Genre</form:option>
						<form:options items="${genres}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="genre.id" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="genreRecherche.id">Genre recherché : </form:label>
					<form:select class="form-control" path="genreRecherche.id">
						<form:option value="">Genre recherché</form:option>
						<form:options items="${genres}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="genreRecherche.id" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="statut.id">Statue : </form:label>
					<form:select class="form-control" path="statut.id">
						<form:option value="">Statue</form:option>
						<form:options items="${statuts}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="statut.id" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="interets">Interets : </form:label>
					<form:select class="form-control" path="interets" multiple="true" >
						<form:options items="${interets}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="interets" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="bio">Bio :</form:label>
					<form:input class="form-control" path="bio"/>
					<form:errors path="bio" cssClass="erreur" />
				</div>
								
				<div class="form-group">
					<form:label path="dateDeNaissance">Date de naissance :</form:label>
					<form:input class="form-control" path="dateDeNaissance" type="date"/>
					<form:errors path="dateDeNaissance" cssClass="erreur" />
				</div>
				
				<form:button class="btn btn-success my-3">Inscription</form:button>
			</form:form>
						
			<a href="index" class="btn btn-primary">Retourner à connexion</a>
		</main>
	</body>
</html>