<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Accueil</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
		<main>

			<jsp:include page="componants/header.jsp"></jsp:include>
			
			<h3>Liste des personnes</h3>
			
		    <form action="accueil" method="post" class="d-flex my-3">
		    	<div class="form-group">
		            <select name="ID_INTERET" class="form-control">
		                <option value="">Interet</option>
		                <c:forEach var="interet" items="${interets}">
		                    <option value="${interet.id}"<c:if test="${interet.id eq interetRecherche.id}"> SELECTED</c:if>>${interet.nom}</option>
		                </c:forEach>
		            </select>
		    	</div>
	            <div class="form-group">
		            <select name="ID_VILLE" class="form-control">
		                <option value="">Ville</option>
		                <c:forEach var="ville" items="${villes}">
		                    <option value="${ville.id}"<c:if test="${ville.id eq villeRecherchee.id}"> SELECTED</c:if>>${ville.nom}</option>
		                </c:forEach>
		            </select>
		        </div>
		        <button type="submit" class="btn btn-primary btn-sm">Filtrer</button>
		    </form>
			
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th><a href="?sort=pseudo,asc"class="dropdown-toggle">Pseudo</a></th>
						<th><a href="?sort=statut.nom,asc"class="dropdown-toggle">Statue</a></th>
						<th><a href="?sort=ville.nom,asc"class="dropdown-toggle">Ville</a></th>
						<th>Opérations</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="personne" items="${personnesParPages.content}">
						<tr>
							<td class="align-middle">${personne.pseudo}</td>
							<td class="align-middle">${personne.statut.nom}</td>
							<td class="align-middle">${personne.ville.nom}</td>
							<td class="align-middle">
								<a href="profil?ID=${personne.id}" class="btn btn-primary btn-sm">Voir</a>
								<a href="inviter?ID=${personne.id}" class="btn btn-primary btn-sm">Inviter</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<a href="liste-invitation" class="btn btn-primary btn-sm">Liste des invitations</a>
			
			<nav aria-label="Page navigation example" class="my-3">
				<ul class="pagination">
					<c:if test="${!personnesParPages.isFirst()}">
				    	<li class="page-item">
				     		<a class="page-link double" href="accueil?page=0&sort=${sort}&ID_VILLE=${villeRecherchee.id}&ID_INTERET=${interetRecherche.id}" aria-label="Previous">
				       	 		<span aria-hidden="true">&laquo;&laquo;</span>
				    		</a>
				    	</li>
				    	<li class="page-item">
				     		<a class="page-link" href="accueil?page=${personnesParPages.number-1}&sort=${sort}&ID_VILLE=${villeRecherchee.id}&ID_INTERET=${interetRecherche.id}" aria-label="Previous">
				       	 		<span aria-hidden="true">&laquo;</span>
				    		</a>
				    	</li>
					</c:if>
			    	<li class="page-item"><a class="page-link" href="#">${personnesParPages.getNumber()+1}</a></li>
			    	<c:if test="${!personnesParPages.isLast()}">
				    	<li class="page-item">
				      		<a class="page-link" href="accueil?page=${personnesParPages.number+1}&sort=${sort}&ID_VILLE=${villeRecherchee.id}&ID_INTERET=${interetRecherche.id}" aria-label="Next">
				      			<span aria-hidden="true">&raquo;</span>
				     		</a>
				  		</li>
				    	<li class="page-item">
				      		<a class="page-link double" href="accueil?page=${personnesParPages.getTotalPages() - 1}&sort=${sort}&ID_VILLE=${villeRecherchee.id}&ID_INTERET=${interetRecherche.id}" aria-label="Next">
				      			<span aria-hidden="true">&raquo;&raquo;</span>
				     		</a>
				  		</li>
			    	</c:if>
			  	</ul>
			</nav>
			
		</main>
	</body>
</html>